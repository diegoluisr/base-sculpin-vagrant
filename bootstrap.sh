#!/usr/bin/env bash

# Variables

# PHPDIR=/etc/php5
PHPDIR=/etc/php/7.0

echo -e "\n--- We are ready! ---\n"
echo -e "\n--- installing ... ---\n"

echo -e "\n--- Updating packages list ---\n"
apt-get -qq update

echo -e "\n--- Install base packages ---\n"
apt-get -y install curl build-essential unzip git > /dev/null 2>&1

echo -e "\n--- Updating packages list - Again ---\n"
apt-get -qq update

echo -e "\n--- Installing PHP packages ---\n"
apt-get -y install php php-curl php-gd php-mcrypt php-apcu php-xml php-mbstring > /dev/null 2>&1

echo -e "\n--- Enabling PHP error reporting ---\n"
sed -i "s/error_reporting = .*/error_reporting = E_ALL/" $PHPDIR/apache2/php.ini
sed -i "s/display_errors = .*/display_errors = On/" $PHPDIR/apache2/php.ini

echo -e "\n--- Installing PHPUnit ---\n"
# version ~5.7
wget https://phar.phpunit.de/phpunit.phar > /dev/null 2>&1
chmod +x phpunit.phar
mv phpunit.phar /usr/local/bin/phpunit

echo -e "\n--- Installing Composer for PHP package management ---\n"
curl --silent https://getcomposer.org/installer | php > /dev/null 2>&1
chmod +x composer.phar
mv composer.phar /usr/local/bin/composer

